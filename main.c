/**
*@file  source.cpp
*@author ����� �.�, ��. 515�
*@date 24 ���� 2019
*@brief ������� ��������
*�������� ������.
*/

#include <stdio.h>
#include <locale.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>
#define maximum 10
void input(int *mas,int n);
void sort_sl(int *mas, int n);
void sort_sch(int *mas, int n, int min, int max);
static void swap(unsigned *mas, unsigned *n);
static void sort_rad(unsigned *from, unsigned *to, unsigned bit);
void sort_raz(int *mas, const size_t len);
int main(void)
{
setlocale(LC_CTYPE, "rus");
int n;
printf("������� ���������� ��������� �������:");
scanf("%d",&n);
int key;
int *mas;
mas = (int *)malloc(n * sizeof(int));
input(mas,n);
printf("�������� ����� ���������� �������\n");
printf("0-���������� ��������;1-���������� ���������;2-����������� ����������;\n");
scanf("%d",&key);
if(key<0 || key>2){
printf("������� ������� �� ��������\n");
return 0;
}
switch (key){
case 0:
sort_sl(mas, n);
  break;
case 1:
    sort_sch(mas,n,0,maximum);
break;
case 2:
   sort_raz(mas,n);
    break;
}
  for (int i=0;i<n;i++)
 {
 printf("%d-� ������� ������� ����� ����������:%d\n",i,mas[i]);
 }
  return 0;
}

void input(int *mas,int n){
int v;
printf("������ ������ �������� ������� � ������?\n ");
 printf("0-��;1-���;\n ");
 scanf("%d",&v);
 if (v<0 || v>1){
printf("������� ������� �����������");
 _Exit (EXIT_SUCCESS);
 }
if (v == 0){
 for(int i=0;i<n;i++){
 printf("������� ������� �������:");
 scanf("%d",&mas[i]);
 }
 }

else{
    {
 srand((unsigned)time(NULL));
 for (int i=0;i<n;i++)
 {
 mas[i]=rand()%10;
 printf("������� �������(%d):%d\n",i,mas[i]);
 }

 }
}
}
void sort_sl(int *mas, int n){
  int step = 1;
  int *temp = (int*)malloc(n * sizeof(temp));
  while (step < n)
  {
    int index = 0;
    int l = 0;
    int m = l + step;
    int r = l + step * 2;
    do
    {
      m = m < n ? m : n;
      r = r < n ? r : n;
      int i1 = l, i2 = m;
      for (; i1 < m && i2 < r; )
      {
        if (mas[i1] < mas[i2]) { temp[index++] = mas[i1++]; }
        else { temp[index++] = mas[i2++]; }
      }
      while (i1 < m) temp[index++] = mas[i1++];
      while (i2 < r) temp[index++] = mas[i2++];
      l += step * 2;
      m += step * 2;
      r += step * 2;
    } while (l < n);
    for (int i = 0; i < n; i++)
      mas[i] = temp[i];
    step *= 2;
  }
}
void sort_sch(int *mas, int n, int min, int max){
    int i, j, z;

  int range = max - min + 1;
  int *count = malloc(range * sizeof(*mas));

  for(i = 0; i < range; i++) count[i] = 0;
  for(i = 0; i < n; i++) count[ mas[i] - min ]++;

  for(i = min, z = 0; i <= max; i++) {
    for(j = 0; j < count[i - min]; j++) {
      mas[z++] = i;
    }
  }

  free(count);
}
static void swap(unsigned *mas, unsigned *n) {
    unsigned tmp = *mas;
    *mas = *n;
    *n = tmp;
}
static void sort_rad(unsigned *from, unsigned *to, unsigned bit){
	if (!bit || to < from + 1) return;

	unsigned *ll = from, *rr = to - 1;
	for (;;) {

		while (ll < rr && !(*ll & bit)) ll++;
		while (ll < rr &&  (*rr & bit)) rr--;
		if (ll >= rr) break;
		swap(ll, rr);
	}

	if (!(bit & *ll) && ll < to) ll++;
	bit >>= 1;

	sort_rad(from, ll, bit);
	sort_rad(ll, to, bit);
}
void sort_raz(int *mas, const size_t len){
	size_t i;
	unsigned *x = (unsigned*) mas;

	for (i = 0; i < len; i++)
            x[i] ^= INT_MIN;

        sort_rad(x, x + len, INT_MIN);

        for (i = 0; i < len; i++)
            x[i] ^= INT_MIN;
}
